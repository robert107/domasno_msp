package iu.keyboard;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class KReading {

	public static String readString() {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String str = "";
		try {
			str = in.readLine();
		} catch (IOException e) {
			System.err.println("Error while reading from keyboard.");
			e.printStackTrace();
		}
		return str;
	}

	public static int readInt() {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int i = 0;
		try {
			i = Integer.parseInt(in.readLine());
		} catch (NumberFormatException e) {
			System.out.println("Error while parsing number.");
			e.printStackTrace();
			return 0;

		} catch (IOException e) {
			System.out.println("Error while reading from keyboard.");
			e.printStackTrace();
			return 0;
		}
		return i;
	}

}