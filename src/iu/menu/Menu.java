package iu.menu;

import iu.keyboard.KReading;

import iu.calculator.Calculator;

public class Menu {
	
	private void show() {
		System.out.println("\nThis is main menu for calculations:");
		System.out.println("1. add two numbers");
		System.out.println("2. subtract two numbers");
		System.out.println("3. divide two numbers");
		System.out.println("4. multiply two numbers");
		System.out.println("_________________");
		System.out.println("0. exit");
		System.out.println("\nPlease enter your shoice: ");
	}

	private void executeOption(int option) {
		switch (option) {
		case 1:
			// add two numbers
			System.out.println("Implement 1: reading two numbers, then calculate, and finally display result.");
			//TODO do this in separate mathod/class
			System.out.print("Insert x = ");
			int x = KReading.readInt();
			
			System.out.print("Insert y = ");
			int y = KReading.readInt();
			
			Calculator calc = new Calculator();
			int z = calc.add(x, y);
			
			System.out.println("The result is:");
			System.out.print(x + " + " + y + " = " + z);
			break;
		case 2:
			// subtract two numbers
			System.out.println("Implement 1: reading two numbers, then calculate, and finally display result.");
			break;
		case 3:
			// divide two numbers
			System.out.println("Implement 1: reading two numbers, then calculate, and finally display result.");
			break;
		case 4:
			// multiply two numbers
			System.out.println("Implement 1: reading two numbers, then calculate, and finally display result.");
			break;
		case 0:
			System.out.println("You coosed exit.");
			break;
			
		default:
			System.out.println("Unsupported menu option value.");
		}

	}
	

	public void execute() {
		int choosedMenuOption = -1; // negative value means option not choosed yet		
		do {
			show();
			choosedMenuOption = KReading.readInt(); // read menu option from keyboard
			executeOption(choosedMenuOption);
		} while (choosedMenuOption != 0);
	}

}
